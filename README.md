These logos are based on Unity theme and created to replace Ubuntu branding. 
The original image is borrowed from https://trisquel.info/en/forum/launcher-icon-unity and modified by me.

## screenshot
![https://devel.trisquel.info/salman/trisquel-logos-for-unity/raw/master/screenshot/trisquel-branding-on-unity.png](https://devel.trisquel.info/salman/trisquel-logos-for-unity/raw/master/screenshot/trisquel-branding-on-unity.png)
